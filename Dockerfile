FROM node:0.10

WORKDIR /src
COPY . .

RUN npm install

EXPOSE 3000
CMD ["node", "app.js"]

