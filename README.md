# marscraft

A multiplayer 2d shooting game to be played on a browser over websockets. Built on Node.js and socket.io.

The first player who joins the game goes to Humans team, the next one to Aliens, then to Humans, and so on. 

One of my goals was to create a small model-agnostic utility library for transporting data between game clients, taking care of lag and stuff. This game uses its initial version (netword.js). The library has still work to be done.

## Running

Locally: Just type `node app.js` and go to `localhost:3000` with multiple tabs.

There is also a Dockerfile if you want to run the app in a Docker container.

## Screenshots

![Marscraft](screenshot.png)
