var express = require('express'),
	http = require('http'),
	path = require('path'),
	_ = require('underscore'),
	app = express(),
	server = http.createServer(app),
	netword = require('./netword'),
	fs = require('fs'),
	readline = require('readline');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
	res.sendfile(__dirname + '/index.html');
});

var conf = {
	mapWidth: 600,
	mapHeight: 600,
	bases: {
		human: {
			x: 70,
			y: 70
		},
		devourer: {
			x: 530,
			y: 530
		}
	}
};

var protos = {
	rifleman: {
		speed: 50.0,
		maxHp: 100.0,
		dmg: 30.0,
		shootDelay: 200,
		bulletSpeed: 300.0,
		rad: 20,
		evolution: 'blaster'
	},
	blaster: {
		speed: 65.0,
		maxHp: 135.0,
		dmg: 30.0,
		rad: 20,
		shootDelay: 130,
		bulletSpeed: 300.0,
		evolution: 'punisher'
	},
	punisher: {
		speed: 80.0,
		maxHp: 175.0,
		dmg: 30.0,
		rad: 20,
		shootDelay: 130,
		bulletSpeed: 450.0
	},
	bug: {
		speed: 170.0,
		maxHp: 30.0,
		dmg: 6.0,
		rad: 5,
		evolution: 'ripper'
	},
	ripper: {
		speed: 125.0,
		maxHp: 200.0,
		dmg: 16.0,
		rad: 10,
		evolution: 'flizard'
	},
	flizard: {
		speed: 80.0,
		maxHp: 500.0,
		dmg: 16.0,
		rad: 25,
	},
	bullet: {
		rad: 2
	},
	ground: {
		rad: 20,
		w: 40,
		h: 40
	}
};

var baseTypes = {
	'human': 'rifleman',
	'devourer': 'bug'
};

var map = [];
var walls = [];
var y = 0;
fs.readFileSync('./map.dat')
.toString()
.split('\n')
.forEach(function (line) {
	var row = [];
	for (var x = 0; x < line.length; x++) {
		var c = line[x];
		row.push(evalGroundSymbol(c, x, y))
	}
	map.push(row);
	y++;
});

function evalGroundSymbol(c, x, y) {
	var ground = {
		'.': {
			tile: 'gravel' + Math.round(Math.random()),
			wall: false
		},
		'w': {
			tile: 'wall',
			wall: true,
		},
		'f': {
			tile: 'floor',
			wall: false,
		},
		't': {
			tile: 'tree',
			wall: true,
		}
	}[c];

	ground.type = 'ground';
	ground.x = x*40;
	ground.y = y*40;

	if (ground.wall)
		walls.push(ground);

	return ground;
}

function onInit(model) {
	model.players = [];
	model.bullets = [];
	model.map = map;
}

function handleEvent(model, event) {
	var player = _.findWhere(model.players, { id: event.socketId });
	if (!player)
		return;

	var name = event.action.name;

	if (name === 'start-move') {
		var dir = event.action.direction;
		if (!_.contains(player.moving, dir)) {
			player.moving.push(dir);
		}
	}
	
	if (name === 'stop-move') {
		var dir = event.action.direction;
		rmFromArray(dir, player.moving);
	}

	if (name === 'start-shoot') {
		if (player.race === 'human')
			player.shooting = true;
	}

	if (name === 'stop-shoot') {
		if (player.race === 'human')
			player.shooting = false;
	}

	if (name === 'angle') {
		player.angle = event.action.angle;
	}

	if (name === 'change-name') {
		player.name = event.action.newName;
	}

	if (name === 'chat-message') {
		var msg = {
			timestamp: new Date(),
			name: player.name,
			msg: event.action.msg
		};

		netword.broadcast('chat-message', msg);
	}
}

function shoot(model, player) {
	player.lastShoot = new Date().getTime();

	model.bullets.push({
		type: 'bullet',
		shooter: player.id,
		x: player.x,
		y: player.y,
		angle: player.angle
	});
}

function hasSpawnProtection(player) {
	return new Date().getTime() - player.spawned < 2000;
}

function hit(model, player, bullet) {
	if (hasSpawnProtection(player))
		return;

	var shooter = _.findWhere(model.players, { id: bullet.shooter });
	var shooterProto = protos[shooter.type];
	player.hp -= shooterProto.dmg;

	if (player.hp <= 0) {
		kill(shooter, player);
	}

	rmFromArray(bullet, model.bullets);
}

function rmFromArray(obj, array) {
	var index = array.indexOf(obj);
	if (index > -1)
		array.splice(index, 1);
}

function devour(devourer, human) {
	if (hasSpawnProtection(human))
		return;

	var devProto = protos[devourer.type];
	human.hp -= devProto.dmg;

	if (human.hp <= 0) {
		kill(devourer, human);
	}
}

function kill(killer, killed) {
	var killerProto = protos[killer.type];
	killer.frags++;
	killed.type = baseTypes[killed.race];
	killed.hp = protos[killed.type].maxHp;
	killed.x = conf.bases[killed.race].x + Math.random() * 50 - 25;
	killed.y = conf.bases[killed.race].y + Math.random() * 50 - 25;
	killed.angle = 0;
	killed.spawned = new Date().getTime();
	if (killerProto.evolution) {
		killer.type = killerProto.evolution;
		killer.hp = protos[killer.type].maxHp;
	}
}

function updateModel(model, ms) {
	if (ms <= 0)
		return;

	var humans = _.filter(model.players, function (player) {
		return player.race === 'human';
	});

	_.each(model.players, function (player) {
		var angle = player.angle;
		var proto = protos[player.type];
		var factor = proto.speed * ms / 1000.0;

		var movings = _.clone(player.moving);
		if (_.contains(movings, 'right') && _.contains(movings, 'left')) {
			rmFromArray('right', movings);
			rmFromArray('left', movings);
		}
		if (_.contains(movings, 'up') && _.contains(movings, 'down')) {
			rmFromArray('up', movings);
			rmFromArray('down', movings);
		}
		if (movings.length > 1) {
			factor /= Math.sqrt(2);
		}


		var oldX = player.x;
		if (_.contains(player.moving, 'right')) {
			player.x += factor;
		}
		if (_.contains(player.moving, 'left')) {
			player.x -= factor;
		}

		if (player.type !== 'flizard') {
			var playerSq = centeredObjToSquare(player);
			_.each(walls, function (wall) {
				if (squaresCollide(playerSq, objToSquare(wall))) {
					player.x = oldX;
				}
			});
		}
		
		var oldY = player.y;
		if (_.contains(player.moving, 'up')) {
			player.y -= factor;
		}
		if (_.contains(player.moving, 'down')) {
			player.y += factor;
		}

		if (player.type !== 'flizard') {
			var playerSq = centeredObjToSquare(player);
			_.each(walls, function (wall) {
				if (squaresCollide(playerSq, objToSquare(wall))) {
					player.y = oldY;
				}
			});
		}

		if (player.x > conf.mapWidth - proto.rad)
			player.x = conf.mapWidth - proto.rad;
		if (player.x < proto.rad)
			player.x = proto.rad;

		if (player.y > conf.mapHeight - proto.rad)
			player.y = conf.mapHeight - proto.rad;
		if (player.y < proto.rad)
			player.y = proto.rad;

		var now = new Date().getTime();
		if (player.shooting && now - player.lastShoot > proto.shootDelay) {
			shoot(model, player);
		}

		if (player.race === 'devourer') {
			_.each(humans, function (human) {
				if (distance(player, human) < proto.rad + protos[human.type].rad) {
					devour(player, human);
				}
			});
		}
	});

	var toBeRemoved = [];
	var bulletProto = protos.bullet;
	_.each(model.bullets, function (bullet) {
		if (!bullet)
			return;

		var shooter = _.findWhere(model.players, { id: bullet.shooter });
		if (!shooter) {
			toBeRemoved.push(bullet);
		} else {
			bullet.x -= Math.cos(bullet.angle) * protos[shooter.type].bulletSpeed * ms / 1000.0;
			bullet.y -= Math.sin(bullet.angle) *protos[shooter.type].bulletSpeed * ms / 1000.0;

			if (bullet.x < 0 || bullet.x > conf.mapWidth || bullet.y < 0 || bullet.y > conf.mapHeight)
				toBeRemoved.push(bullet);

			_.each(walls, function (wall) {
				if (bullet.x > wall.x && bullet.x < wall.x + 40 &&
					bullet.y > wall.y && bullet.y < wall.y + 40)
					toBeRemoved.push(bullet);
			});


			_.each(model.players, function (player) {
				var playerRad = protos[player.type].rad;
				if (distance(bullet, player) < bulletProto.rad + playerRad && bullet.shooter !== player.id) {
					hit(model, player, bullet);
				}
			});
		}
	});

	_.each(toBeRemoved, function (b) {
		rmFromArray(b, model.bullets);
	});
}

function centeredObjToSquare(o) {
	var proto = protos[o.type];
	return {
		x1: o.x - proto.rad * 0.8,
		x2: o.x + proto.rad * 0.8,
		y1: o.y - proto.rad * 0.8,
		y2: o.y + proto.rad * 0.8 
	};
}

function objToSquare(o) {
	var proto = protos[o.type];
	return {
		x1: o.x,
		x2: o.x + proto.w,
		y1: o.y,
		y2: o.y + proto.h 
	};
}

function squaresCollide(sq1, sq2) {
	return sq1.x1 < sq2.x2 && sq1.x2 > sq2.x1 && sq1.y1 < sq2.y2 && sq1.y2 > sq2.y1;
}

function distance(m1, m2) {
	return Math.sqrt((m1.x - m2.x) * (m1.x - m2.x) + (m1.y - m2.y) * (m1.y - m2.y));
}

function addPlayer(model, id, settings) {
	console.log('Player connected: ', id);

	var humanCount = _.filter(model.players, function (player) {
		return player.race === 'human';
	}).length;

	var devourerCount = _.filter(model.players, function (player) {
		return player.race === 'devourer';
	}).length;

	var race, type;
	if (humanCount > devourerCount) {
		race = 'devourer';
		type = 'bug';
	} else {
		race = 'human';
		type = 'rifleman';
	}

	model.players.push({
		id: id,
		race: race,
		type: type,
		name: settings.name,
		x: conf.bases[race].x + Math.random() * 50 - 25,
		y: conf.bases[race].y + Math.random() * 50 - 25,
		hp: protos[type].maxHp,
		moving: [],
		angle: 0.0,
		lastShoot: 0,
		frags: 0,
		spawned: new Date().getTime()
	});
}

function removePlayer(model, id) {
	console.log('Player left: ', id);
	var updatedPlayers = _.filter(model.players, function (player) {
		return player.id !== id;
	});
	model.players = updatedPlayers;
}

netword.init({
	server: server,
	onInit: onInit,
	onNewConnection: addPlayer,
	onDisconnection: removePlayer,
	onModelUpdate: updateModel,
	onEvent: handleEvent
});

server.listen(3000, function() {
	console.log('Express server listening at ' + 3000);
});