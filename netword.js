var _ = require('underscore'),
	ioLib = require('socket.io');

var io,
	opts,
	model = {},
	prevModel,
	pendingEvents = [],
	now,
	last = Date.now(),
	broadcastTimer,
	broadcastInterval = 70;

function deepClone(obj) {
	return JSON.parse(JSON.stringify(obj));
}

function calcLag(samples) {
	var totalAvg = _.reduce(samples, function (memo, sample) {
		return memo + sample;
	}) / samples.length;

	var filtered = _.filter(samples, function (sample) {
		return Math.abs(sample - totalAvg) < totalAvg;
	});

	var avg = _.reduce(filtered, function (memo, sample) {
		return memo + sample;
	}) / filtered.length;
	return Math.round(avg);
}

function start() {
	opts.onInit(model);
	map = model.map;
	delete model.map;

	io.on('connection', function (socket) {
		socket.on('disconnect', function () {
			opts.onDisconnection(model, socket.id);
		});

		socket.emit('map', map);

		var pingTime, lag = 0, lagSamples = [];
		socket.on('settings', function (settings) {
			socket.on('event', function (event) {
				var realTimestamp = Date.now() - lag;

				pendingEvents.push({
					socketId: event.socketId,
					action: event.action,
					realTimestamp: realTimestamp
				});
			});

			socket.on('pong', function () {
				lagSample = (Date.now() - pingTime) / 2.0;
				addLagSample(lagSample);
				lag = calcLag(lagSamples);
				socket.emit('lag', lag);
			});

			function addLagSample(sample) {
				var maxSamples = 7;

				lagSamples.push(sample);
				var len = lagSamples.length;
				if (len > maxSamples) {
					lagSamples = lagSamples.slice(len - maxSamples, len);
				}
			}

			function emitPing() {
				pingTime = Date.now();
				socket.emit('ping');
				setTimeout(emitPing, 500);
			}
			emitPing();

			opts.onNewConnection(model, socket.id, settings);

			if (!broadcastTimer) {
				broadcastTimer = setInterval(function () {
					last = now;
					now = Date.now();
					var diff = now - last;

					update(diff);
					io.sockets.emit('update', model);
				}, broadcastInterval);
			}
		});
	});
}

function update(ms) {
	var sortedEvents = _.sortBy(deepClone(pendingEvents), function (event) {
		return event.realTimestamp;
	});
	pendingEvents = [];

	var newModel = deepClone(model);
	var internalTime = 0;
	_.each(sortedEvents, function (event) {
		var diff = Math.max(event.realTimestamp - last, 0);
		var diffsDiff = diff - internalTime
		internalTime += diffsDiff;

		opts.onModelUpdate(newModel, diffsDiff);
		model = newModel;
		opts.onEvent(model, event);
	});
	if (internalTime < broadcastInterval) {
		model = opts.onModelUpdate(newModel, broadcastInterval - internalTime);
		model = newModel;
	}
}

exports.init = function (_opts) {
	opts = _opts;
	io = ioLib(opts.server);

	start();
};

exports.broadcast = function (name, obj) {
	io.sockets.emit('misc', {
		name: name,
		obj: obj
	});
};