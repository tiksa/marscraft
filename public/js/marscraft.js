var Marscraft = (function () {
	var ctx,
		canvasElem,
		controlsActive = false,
		cursorSet = false,
		parentX,
		parentY,
		map,
		walls,
		camera = {
			x: 0,
			y: 0,
			w: 400,
			h: 400
		},
		conf = {
			mapWidth: 600,
			mapHeight: 600,
			bases: {
				human: {
					x: 70,
					y: 70
				},
				devourer: {
					x: 530,
					y: 530
				}
			}
		},
		moveKeys = [{
			code: 87,
			name: 'up'
		}, {
			code: 83,
			name: 'down'
		}, {
			code: 65,
			name: 'left'
		}, {
			code: 68,
			name: 'right'
		}],
		riflemanImg = new Image(),
		blasterImg = new Image(),
		punisherImg = new Image(),
		bugImg = new Image(),
		ripperImg = new Image(),
		flizardImg = new Image(),
		bulletImg = new Image(),
		gravel0Img = new Image(),
		gravel1Img = new Image(),
		wallImg = new Image(),
		floorImg = new Image(),
		treeImg = new Image(),
		bulletWav = loadAudio('wav/gun.wav'),
		arghWav = loadAudio('wav/argh.wav'),
		viuWav = loadAudio('wav/viu.wav'),
		lastShoot = 0,
		protos = {
			rifleman: {
				speed: 50.0,
				maxHp: 100.0,
				dmg: 30.0,
				shootDelay: 200,
				bulletSpeed: 300.0,
				rad: 20,
				img: riflemanImg,
				deathWav: arghWav,
				centered: true,
				rotated: true
			},
			blaster: {
				speed: 65.0,
				maxHp: 135.0,
				dmg: 30.0,
				rad: 20,
				shootDelay: 150,
				bulletSpeed: 300.0,
				img: blasterImg,
				deathWav: arghWav,
				centered: true,
				rotated: true
			},
			punisher: {
				speed: 80.0,
				maxHp: 175.0,
				dmg: 30.0,
				rad: 20,
				shootDelay: 130,
				bulletSpeed: 450.0,
				img: punisherImg,
				deathWav: arghWav,
				centered: true,
				rotated: true
			},
			bug: {
				speed: 170.0,
				maxHp: 30.0,
				dmg: 6.0,
				rad: 5,
				img: bugImg,
				deathWav: viuWav,
				centered: true,
				rotated: true
			},
			ripper: {
				speed: 90.0,
				maxHp: 200.0,
				dmg: 16.0,
				rad: 10,
				img: ripperImg,
				deathWav: viuWav,
				centered: true,
				rotated: true
			},
			flizard: {
				speed: 80.0,
				maxHp: 500.0,
				dmg: 16.0,
				rad: 25,
				img: flizardImg,
				deathWav: viuWav,
				centered: true,
				rotated: true
			},
			bullet: {
				rad: 2,
				img: bulletImg,
				centered: true,
				rotated: false
			},
			ground: {
				rad: 20,
				w: 40,
				h: 40,
				centered: false,
				rotated: false
			}
		};
	
	riflemanImg.src = 'img/rifleman.png';
	blasterImg.src = 'img/blaster.png';
	punisherImg.src = 'img/punisher.png';
	bugImg.src = 'img/bug.png';
	ripperImg.src = 'img/ripper.png';
	flizardImg.src = 'img/flizard.png';
	bulletImg.src = 'img/bullet.png';
	gravel0Img.src = 'img/gravel0.png';
	gravel1Img.src = 'img/gravel1.png';
	wallImg.src = 'img/wall.png';
	floorImg.src = 'img/floor.png';
	treeImg.src = 'img/tree.png';

	var imgs = {
		'gravel0': gravel0Img,
		'gravel1': gravel1Img,
		'wall': wallImg,
		'floor': floorImg,
		'tree': treeImg
	};

	function shoot(model, player) {
		lastShoot = new Date().getTime();

		model.bullets.push({
			type: 'bullet',
			shooter: player.id,
			x: player.x,
			y: player.y,
			angle: player.angle
		});

		bulletWav.play();
	}

	function loadAudio(src) {
		var copies = [];
		for (var i = 0; i < 10; i++) {
			copies.push(new Audio(src));
		}

		var current = 0;

		var play = function () {
			copies[current].play();
			current = (current + 1) % copies.length;
		};

		return {
			play: play
		};
	}

	function rmFromArray(obj, array) {
		var index = array.indexOf(obj);
		if (index > -1)
			array.splice(index, 1);
	}

	function devour(devourer, human) {
		if (hasSpawnProtection(human))
			return;

		human.hp -= protos[devourer.type].dmg;

		if (human.hp <= 0) {
			devourer.frags++;
			protos[human.type].deathWav.play();
		}
	}

	function hit(model, player, bullet) {
		if (hasSpawnProtection(player))
			return;

		var shooter = _.findWhere(model.players, { id: bullet.shooter });
		player.hp -= protos[shooter.type].dmg;

		if (player.hp <= 0) {
			shooter.frags++;
			protos[player.type].deathWav.play();
		}

		rmFromArray(bullet, model.bullets);
	}

	function hasSpawnProtection(player) {
		return new Date().getTime() - player.spawned < 2000;
	}

	function updateModel(model, ms) {
		var humans = _.filter(model.players, function (player) {
			return player.race === 'human';
		});

		_.each(model.players, function (player) {
			var angle = player.angle;
			var proto = protos[player.type];
			var factor = proto.speed * ms / 1000.0;

			var movings = _.clone(player.moving);
			if (_.contains(movings, 'right') && _.contains(movings, 'left')) {
				rmFromArray('right', movings);
				rmFromArray('left', movings);
			}
			if (_.contains(movings, 'up') && _.contains(movings, 'down')) {
				rmFromArray('up', movings);
				rmFromArray('down', movings);
			}
			if (movings.length > 1) {
				factor /= Math.sqrt(2);
			}

			var oldX = player.x;
			if (_.contains(player.moving, 'right')) {
				player.x += factor;
			}
			if (_.contains(player.moving, 'left')) {
				player.x -= factor;
			}

			if (player.type !== 'flizard') {
				var playerSq = centeredObjToSquare(player);
				_.each(walls, function (wall) {
					if (squaresCollide(playerSq, objToSquare(wall))) {
						player.x = oldX;
					}
				});
			}
			
			var oldY = player.y;
			if (_.contains(player.moving, 'up')) {
				player.y -= factor;
			}
			if (_.contains(player.moving, 'down')) {
				player.y += factor;
			}

			if (player.type !== 'flizard') {
				var playerSq = centeredObjToSquare(player);
				_.each(walls, function (wall) {
					if (squaresCollide(playerSq, objToSquare(wall))) {
						player.y = oldY;
					}
				});
			}

			if (player.x > conf.mapWidth - proto.rad)
				player.x = conf.mapWidth - proto.rad;
			if (player.x < proto.rad)
				player.x = proto.rad;

			if (player.y > conf.mapHeight - proto.rad)
				player.y = conf.mapHeight - proto.rad;
			if (player.y < proto.rad)
				player.y = proto.rad;

			if (player.race === 'devourer') {
				_.each(humans, function (human) {
					if (distance(player, human) < proto.rad) {
						devour(player, human);
					}
				});
			}

			var now = new Date().getTime();
			if (player.shooting && now - lastShoot > proto.shootDelay) {
				shoot(model, player);
			}
		});

		var bulletProto = protos.bullet;
		_.each(model.bullets, function (bullet) {
			if (!bullet)
				return;

			var shooter = _.findWhere(model.players, { id: bullet.shooter });
			if (!shooter)
				return;

			bullet.x -= Math.cos(bullet.angle) * protos[shooter.type].bulletSpeed * ms / 1000.0;
			bullet.y -= Math.sin(bullet.angle) *protos[shooter.type].bulletSpeed * ms / 1000.0;

			var toBeRemoved = [];
			_.each(walls, function (wall) {
				if (bullet.x > wall.x && bullet.x < wall.x + 40 &&
					bullet.y > wall.y && bullet.y < wall.y + 40)
					toBeRemoved.push(bullet);
			});

			_.each(toBeRemoved, function (bullet) {
				rmFromArray(bullet, model.bullets);
			});

			_.each(model.players, function (player) {
				var proto = protos[player.type];
				if (distance(bullet, player) < bulletProto.rad + proto.rad && bullet.shooter !== player.id) {
					hit(model, player, bullet);
				}
			});
		});

		updateCamera(model);

		if (!cursorSet) {
			setCursor(model);
		}

		draw(model);
	}

	function setCursor(model) {
		var	id = Netword.getId(),
			me = _.findWhere(model.players, { id: id });

		if (!me)
			return;

		if (me.race === 'human')
			$(canvasElem).css('cursor', 'url("../img/target.png") 10 10, auto')
		else
			$(canvasElem).css('cursor', 'none');
		cursorSet = true;
	}

	function updateCamera(model) {
		var	id = Netword.getId(),
			me = _.findWhere(model.players, { id: id });

		if (!me)
			return;

		camera.x = me.x;
		camera.y = me.y;

		if (camera.x < camera.w / 2)
			camera.x = camera.w / 2;
		if (camera.x > conf.mapWidth - camera.w / 2)
			camera.x = conf.mapWidth - camera.w / 2;
		if (camera.y < camera.h / 2)
			camera.y = camera.h / 2;
		if (camera.y > conf.mapHeight - camera.h / 2)
			camera.y = conf.mapHeight - camera.h / 2;
	}

	function camX(x) {
		return x - camera.x + camera.w / 2;
	}

	function camY(y) {
		return y - camera.y + camera.h / 2;
	}

	function isInsideCam(o, proto) {
		return Math.abs(o.x - camera.x) < camera.w / 2  + proto.rad * 2 &&
			Math.abs(o.y - camera.y) < camera.h / 2 + proto.rad * 2;
	}

	function centeredObjToSquare(o) {
		var proto = protos[o.type];
		return {
			x1: o.x - proto.rad * 0.8,
			x2: o.x + proto.rad * 0.8,
			y1: o.y - proto.rad * 0.8,
			y2: o.y + proto.rad * 0.8 
		};
	}

	function objToSquare(o) {
		var proto = protos[o.type];
		return {
			x1: o.x,
			x2: o.x + proto.w,
			y1: o.y,
			y2: o.y + proto.h 
		};
	}

	function squaresCollide(sq1, sq2) {
		return sq1.x1 < sq2.x2 && sq1.x2 > sq2.x1 && sq1.y1 < sq2.y2 && sq1.y2 > sq2.y1;
	}

	function distance(m1, m2) {
		return Math.sqrt((m1.x - m2.x) * (m1.x - m2.x) + (m1.y - m2.y) * (m1.y - m2.y));
	}

	function drawObj(o) {
		var proto = protos[o.type];

		if (!isInsideCam(o, proto))
			return;

		var	cx = camX(o.x),
			cy = camY(o.y),
			img = o.img || proto.img || imgs[o.tile];

		if (proto.rotated && proto.centered) {
			if (hasSpawnProtection(o))
				drawRotatedImage(img, cx, cy, o.angle, 0.25);
			else
				drawRotatedImage(img, cx, cy, o.angle);
		} else if (!proto.rotated && proto.centered) {
			drawCenteredImage(img, cx, cy);
		} else if (!proto.rotated && !proto.centered) {
			drawImage(img, cx, cy);
		}

		if (o.race) {
			ctx.textAlign = 'center';
			ctx.fillStyle = "#FFFFFF";
			ctx.fillText(o.name, cx, cy + proto.rad / 2 + 14);

			ctx.fillStyle = "#00DD00";
			ctx.fillRect(cx - 30, cy - proto.rad - 10, o.hp / proto.maxHp * 60, 4);
		}
	}

	function drawImage(img, x, y, alpha) {
		if (alpha)
			ctx.globalAlpha = 0.5;
		
		ctx.drawImage(img, x, y);
		
		if (alpha)
			ctx.globalAlpha = 1.0;
	}

	function drawCenteredImage(img, x, y) {
		drawImage(img, x - img.width / 2, y - img.height / 2);
	}

	function drawRotatedImage(img, x, y, angle, alpha) {
		ctx.save();
		ctx.translate(x, y);
		ctx.rotate(angle);
		drawImage(img, - img.width / 2, - img.height / 2, alpha);
		ctx.restore();
	}

	function draw(model) {
		if (!map)
			map = Netword.getMap();

		if (!map) {
			return;
		} else {
			if (!walls) {
				walls = [];
				_.each(map, function (row) {
					_.each(row, function (ground) {
						if (ground.wall)
							walls.push(ground);
					});
				});
			}
		}

		_.each(map, function (row) {
			_.each(row, function (ground) {
				drawObj(ground);
			});
		});

		_.each(model.bullets, function (bullet) {
			drawObj(bullet);
		});
		
		_.each(model.players, function (player) {
			drawObj(player);
		});

	}

	function onKeyDown(e) {
		var model = Netword.getModel(),
			id = Netword.getId(),
			me = _.findWhere(model.players, { id: id });

		onMoveKey(e.keyCode, me, true);
	}

	function onKeyUp(e) {
		var model = Netword.getModel(),
			id = Netword.getId(),
			me = _.findWhere(model.players, { id: id });

		onMoveKey(e.keyCode, me, false);
	}

	function onMoveKey(code, player, isStart) {
		_.each(moveKeys, function (moveKey) {
			if (code === moveKey.code) {
				var index = player.moving.indexOf(moveKey.name);
				if (isStart) {
					Netword.sendEvent({
						name: 'start-move',
						direction: moveKey.name
					});
					if (index < 0)
						player.moving.push(moveKey.name);
				} else {
					Netword.sendEvent({
						name: 'stop-move',
						direction: moveKey.name
					});
					rmFromArray(moveKey.name, player.moving);
				}
			}
		});
	}

	function onMouseMove(e) {
		var model = Netword.getModel(),
			id = Netword.getId(),
			me = _.findWhere(model.players, { id: id });

		var x = e.pageX - parentX;
		var y = e.pageY - parentY;

		if (!x || !y || !me || x < 0 || x > conf.mapWidth || y < 0 || y > conf.mapHeight) {
			controlsActive = false;
			return;
		}
		controlsActive = true;

		var angle = 0;

		function toDegrees(rad) {
			return rad / (Math.PI) * 180;
		}

		var cx = x + camera.x - camera.w / 2,
			cy = y + camera.y - camera.h / 2;

		angle = Math.atan2(cy - me.y, cx - me.x) + Math.PI;

		Netword.throttledSendEvent({
			name: 'angle',
			angle: angle
		});

	}

	function onMouseDown() {
		if (!controlsActive)
			return;

		var model = Netword.getModel(),
			id = Netword.getId(),
			me = _.findWhere(model.players, { id: id });

		if (me.race !== 'human')
			return;

		Netword.sendEvent({
			name: 'start-shoot',
		});
	}

	function onMouseUp() {
		if (!controlsActive)
			return;

		var model = Netword.getModel(),
			id = Netword.getId(),
			me = _.findWhere(model.players, { id: id });

		if (me.race !== 'human')
			return;

		Netword.sendEvent({
			name: 'stop-shoot',
		});
	}

	function init(_canvasElem) {
		canvasElem = _canvasElem;
		ctx = _canvasElem.getContext('2d');
		var offset = $(_canvasElem).offset();
		parentX = offset.left,
		parentY = offset.top;
	}

	function start() {
		var settings = {
			name: 'unnamed'
		};
		Netword.init({
			onModelUpdate: updateModel,
			onMiscEvent: handleMiscEvent,
			participantSettings: settings
		});

		$(document)
		.on('keydown', onKeyDown)
		.on('keyup', onKeyUp)
		.on('mousemove', onMouseMove)
		.on('mousedown', onMouseDown)
		.on('mouseup', onMouseUp);

		$('.name-input').blur(function () {
			var name = $(this).val();
			Netword.sendEvent({
				name: 'change-name',
				newName: name
			});
		});
	}

	function handleMiscEvent(name, obj) {
		if (name === 'chat-message') {
			addMessage(obj);
		}
	}

	function sendMessage(msg) {
		Netword.sendEvent({
			name: 'chat-message',
			msg: msg
		});
	}

	return {
		init: init,
		start: start,
		sendMessage: sendMessage,
		getModel: function () { return Netword.getModel(); }
	};
})();
