var Netword = (function () {
	var	opts,
		lag = 0,
		updateRate = 30.0,
		socket,
		map,
		model = {},
		trueModelUpdate = {
			model: model,
			realTimestamp: Date.now()
		};

	function deepClone(obj) {
		return JSON.parse(JSON.stringify(obj));
	}

	function updateModel() {
		var now = Date.now();

		var newModel = deepClone(trueModelUpdate.model);
		var diff = now - trueModelUpdate.realTimestamp;

		opts.onModelUpdate(newModel, diff);
		model = newModel;
	}

	function init(_opts) {
		opts = _opts;
		socket = io(window.location.origin);

		socket.on('map', function (_map) {
			map = _map;
			
			socket.on('ping', function () {
				socket.emit('pong');
			});

			socket.on('lag', function (_lag) {
				lag = _lag;
			});

			socket.on('update', function (model) {
				trueModelUpdate = {
					model: model,
					realTimestamp: Date.now() - lag
				}
			});

			socket.on('misc', function (event) {
				opts.onMiscEvent(event.name, event.obj);
			});
			
			socket.emit('settings', opts.participantSettings);

			function update() {
				if (!socket.connected)
					return;

				updateModel();
			}

			setInterval(update, Math.round(1000.0 / updateRate));
		});
	};

	function sendEvent(action) {
		socket.emit('event', {
			socketId: socket.id,
			action: action
		});
	}

	var throttledSendEvent = _.throttle(sendEvent, 60);

	return {
		init: init,
		sendEvent: sendEvent,
		throttledSendEvent: throttledSendEvent,
		getModel: function () {
			return model;
		},
		getId: function () {
			return socket.id;
		},
		getMap: function () {
			return map;
		}
	};
})();